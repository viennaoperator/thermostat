import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OverviewComponent } from './overview/overview.component';
import { DetailviewComponent } from './detailview/detailview.component';

const routes: Routes = [
  { path: '', component: OverviewComponent , pathMatch: 'full' },
  { path: 'thermostat/:id', component: DetailviewComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
