import { Component, Input, OnInit } from '@angular/core';
import { Thermostat } from 'src/app/models/thermostat';
import { Reading } from 'src/app/models/reading';
import { StoreService } from 'src/app/services/store.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-thermostat-tile',
    templateUrl: './thermostat-tile.component.html',
    styleUrls: ['./thermostat-tile.component.scss']
})
export class ThermostatTileComponent implements OnInit {

    @Input() thermostat: Thermostat;
    reading$: Observable<Reading>;

    constructor(private storeService: StoreService) {}

    ngOnInit() {
        this.reading$ = this.storeService.latestReadingForThermostat$(this.thermostat);
    }
}
