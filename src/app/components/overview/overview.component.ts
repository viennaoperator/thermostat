import { Component } from '@angular/core';
import { StoreService } from 'src/app/services/store.service';
import {Observable} from 'rxjs';
import {Thermostat} from '../../models/thermostat';

@Component({
    selector: 'app-overview',
    templateUrl: './overview.component.html',
    styleUrls: ['./overview.component.scss']
})
export class OverviewComponent {

    thermostats$: Observable<Thermostat[]>;

    constructor(private storeService: StoreService) {
      this.thermostats$ = this.storeService.thermostats$;
    }

    /**
     * track by function to reduce dom manipulation
     * only renders dom if object int array has changed
     */
    trackByFn(index: number, item: Thermostat) {
      return item.id;
    }
}
