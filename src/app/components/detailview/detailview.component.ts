import {Component, OnInit, ViewChild} from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { Reading } from '../../models/reading';
import { StoreService } from '../../services/store.service';
import { Observable } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { Thermostat } from '../../models/thermostat';

@Component({
    selector: 'app-detailview',
    templateUrl: './detailview.component.html',
    styleUrls: ['./detailview.component.scss']
})
export class DetailviewComponent implements OnInit {

  id: number;
  thermostat: Thermostat;
  readings$: Observable<Reading[]>;
  displayedColumns: string[] = ['id', 'temperature', 'humidity', 'battery_charge'];

  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;

  constructor(
    private storeService: StoreService,
    private route: ActivatedRoute,
    private router: Router) {
  }

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');
    this.thermostat = this.storeService.findThermostat(this.id);
    if (!this.thermostat) {
      this.navigateBack();
    }
    this.readings$ = this.storeService.readingsForThermostat$(this.thermostat);
  }

  navigateBack() {
    this.router.navigate([`/`]);
  }

}
