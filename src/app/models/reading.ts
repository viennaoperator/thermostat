
export class Reading {
    id: number;
    thermostat_id: number;
    temperature: number;
    humidity: number;
    battery_charge: number;
}