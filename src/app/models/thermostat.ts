import { Address } from './address';

export class Thermostat {
    id: number;
    household_token: string;
    address: Address;
}