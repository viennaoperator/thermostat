
export class Address {
    street: string;
    country: string;
    postal_code: string;
    city: string;
}