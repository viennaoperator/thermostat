import { Injectable } from '@angular/core';
import { Thermostat } from '../models/thermostat';
import { DataService } from './data.service';

/**
 * Service that talks to the API.
 * Service is currently mocked with the data.service.ts
 */
@Injectable()
export class ThermostatService {

    constructor(private dataService: DataService) {}

    public getThermostats() {
        return this.dataService.getThermostats();
    }

    /**
     * This will return ALL readings, careful if this is a big data set
     * then you are going to pull a massive amount of data.
     */
    public getReadings() {
        return this.dataService.getReadings();
    }

    public getLatestReadings() {
      return this.dataService.getLatestReadings();
    }

    public createThermostat(thermostat: Thermostat) {
        console.warn('not talking to the api yet');
    }
}
