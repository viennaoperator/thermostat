import { Thermostat } from '../models/thermostat';
import { BehaviorSubject, Observable } from 'rxjs';
import { Reading } from '../models/reading';
import { Injectable } from '@angular/core';
import { ThermostatService } from './thermostat.service';

/**
 * Service that stores & handles data of application
 */
@Injectable({
  providedIn: 'root',
})
export class StoreService {

    private readonly _thermostats = new BehaviorSubject<Thermostat[]>([]);
    private readonly _readings = new BehaviorSubject<Reading[]>([]);
    private readonly _latestReadings = new BehaviorSubject<Reading[]>([]);

    public readonly thermostats$ = this._thermostats.asObservable();
    public readonly readings$ = this._readings.asObservable();
    public readonly latestReadings$ = this._latestReadings.asObservable();

    constructor(private thermostatService: ThermostatService) {
        this.fetchThermostatsFromBackend();
        this.fetchReadingsFromBackend();
        this.fetchLatestReadingsFromBackend();


        const self = this;
        setTimeout(() => {
          self.fetchReadingsFromBackend();
          self.fetchLatestReadingsFromBackend();
          console.log('new readings fetched');
        }, 1000);
    }

    fetchThermostatsFromBackend() {
        this._thermostats.next(this.thermostatService.getThermostats());
    }

    fetchReadingsFromBackend() {
        // Improvement: Fetch only the ones that I need, given the time constraint I didn't implement this
        this._readings.next(this.thermostatService.getReadings());
    }

    fetchLatestReadingsFromBackend() {
      this._latestReadings.next(this.thermostatService.getLatestReadings());
    }

    latestReadingForThermostat$(thermostat: Thermostat): Observable<Reading> {
        const subject = new BehaviorSubject<Reading>(new Reading());
        const obs = subject.asObservable();
        this.latestReadings$.subscribe(readings => {
            subject.next(readings.find(reading => reading.thermostat_id === thermostat.id));
        });
        return obs;
    }

    readingsForThermostat$(thermostat: Thermostat): Observable<Reading[]> {
      const subject = new BehaviorSubject<Reading[]>([]);
      const obs = subject.asObservable();
      this.readings$.subscribe(readings => {
        subject.next(readings.filter(reading => reading.thermostat_id === thermostat.id));
      });
      return obs;
    }

    addThermostat(thermostat: Thermostat) {
         const currentThermostats = this._thermostats.getValue();
         currentThermostats.push(thermostat);
         this._thermostats.next(currentThermostats);
    }

    findThermostat(id: number): Thermostat {
      return this._thermostats.getValue().find(thermostat => thermostat.id === id);
    }
}
