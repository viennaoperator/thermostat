import { Injectable } from '@angular/core';
import { Thermostat } from '../models/thermostat';
import { Reading } from '../models/reading';

/**
 * Mock service for test data,
 * I planned on having this logic in the backend.
 */
@Injectable()
export class DataService {

    public getThermostats(): Thermostat[] {
        return thermostats;
    }

    public getReadings(): Reading[] {
        return readings;
    }

    public getLatestReadings(): Reading[] {
      const indexedIds: number[] = [];
      const latestReadings = [];
      let i = this.getHighestThermostatId();
      while (true) {
        if (indexedIds.find(id => id === i)) {
          i--;
        }
        if (i < 0) {
          break;
        }

        const finding = readings.find(reading => reading.thermostat_id === i);
        if (finding) {
          indexedIds.push(i);
          latestReadings.push(finding);
        }
        i--;
      }
      return latestReadings;
    }

    public getHighestThermostatId(): number {
       return Math.max.apply(Math, readings.map(r => r.thermostat_id));
    }
}

// Test data
export const thermostats = [
    { 
        id: 1,
        household_token: 'Bath room',
        address: {
            street: 'Schönhauser Allee 149',
            country: 'Germany',
            postal_code: '10435',
            city: 'Berlin'
        }
    },
    { 
        id: 2,
        household_token: 'Living room',
        address: {
            street: 'Schönhauser Allee 149',
            country: 'Germany',
            postal_code: '10435',
            city: 'Berlin'
        }
    },
    { 
        id: 3,
        household_token: 'Kitchen',
        address: {
            street: 'Schönhauser Allee 149',
            country: 'Germany',
            postal_code: '10435',
            city: 'Berlin'
        }
    },
    { 
        id: 4,
        household_token: 'Bath room',
        address: {
            street: 'Knaackstraße',
            country: 'Germany',
            postal_code: '10405',
            city: 'Berlin'
        }
    },
    { 
        id: 5,
        household_token: 'Kitchen',
        address: {
            street: 'Knaackstraße',
            country: 'Germany',
            postal_code: '10405',
            city: 'Berlin'
        }
    },
];

export const readings = [
    {
        id: 1,
        thermostat_id: 1,
        temperature: 20.32,
        humidity: 80,
        battery_charge: 100
    }, 
    {
        id: 2,
        thermostat_id: 1,
        temperature: 18.32,
        humidity: 60,
        battery_charge: 50
    },
    {
        id: 3,
        thermostat_id: 1,
        temperature: 20.32,
        humidity: 40.1,
        battery_charge: 40
    }, 
    {
        id: 4,
        thermostat_id: 1,
        temperature: 20.32,
        humidity: 80,
        battery_charge: 14.58
    }, 
    {
        id: 5,
        thermostat_id: 2,
        temperature: 22,
        humidity: 20,
        battery_charge: 10
    }, 
    {
        id: 6,
        thermostat_id: 2,
        temperature: 24,
        humidity: 30,
        battery_charge: 90
    }, 
];

// latest readings should have the highest number
readings.sort().reverse();
