import {DataService} from './data.service';
import {getTestBed, TestBed} from '@angular/core/testing';

describe('DataService', () => {

  let service: DataService = new DataService();
  let injector: TestBed;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        DataService
      ],
    });
    injector = getTestBed();
    service = injector.get(DataService);
  });

  it('getHighestThermostatId should return highest thermostat id', () => {
    expect(service.getHighestThermostatId()).toEqual(2);
  });

  it('getLatestReadings should return latest readings', () => {
    const latestReadings = service.getLatestReadings().map(reading => reading.id);
    expect(latestReadings).toContain(6);
    expect(latestReadings).toContain(4);
  });
});
