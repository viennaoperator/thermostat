import { StoreService } from './store.service';
import { DataService, thermostats, readings } from './data.service';
import { ThermostatService } from './thermostat.service';
import { Thermostat } from '../models/thermostat';
import { TestBed, getTestBed } from '@angular/core/testing';

describe('StoreService', () => {

    let injector: TestBed;
    let service: StoreService;

    beforeEach(() => {
        TestBed.configureTestingModule({
          providers: [
              StoreService,
              DataService,
              ThermostatService
            ],
        });
        injector = getTestBed();
        service = injector.get(StoreService);
    });

    it('thermostats$ should contain test data', () => {
        service.thermostats$.subscribe(thermostatsInStore => {
            expect(thermostatsInStore).toEqual(thermostats);
        });
    });

    it('readings$ should contain test data', () => {
        service.readings$.subscribe(readingsInStore => {
            expect(readingsInStore).toEqual(readings);
        });
    });

    it('should be possible to add a thermostat', () => {
        // Arrange
        let thermostat: Thermostat = 
            { 
                id: 6,
                household_token: 'token_user2',
                address: {
                    street: 'Knaackstraße',
                    country: 'Germany',
                    postal_code: '10405',
                    city: 'Berlin'
                }
            }
        let thermostatDeepCopy = JSON.parse(JSON.stringify(thermostats));
        let thermostatDeepCopy2 = JSON.parse(JSON.stringify(thermostats));
        thermostatDeepCopy.push(thermostat);

        // Act
        service.addThermostat(thermostat);
        service.thermostats$.subscribe(thermostatsInStore => {

            // Assert
            expect(thermostatsInStore).toEqual(thermostatDeepCopy);
            expect(thermostatsInStore).not.toEqual(thermostatDeepCopy2);
        });
    });
});
