import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './components/app-routing.module';
import { AppComponent } from './components/app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OverviewComponent } from './components/overview/overview.component';
import { DetailviewComponent } from './components/detailview/detailview.component';
import { StoreService } from './services/store.service';
import { DataService } from './services/data.service';
import { ThermostatService } from './services/thermostat.service';
import { ThermostatTileComponent } from './components/overview/thermostat-tile/thermostat-tile.component';
import { MatIconModule } from '@angular/material/icon';
import {MatButtonModule, MatTableModule} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    OverviewComponent,
    DetailviewComponent,
    ThermostatTileComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatTableModule,
    MatButtonModule
  ],
  exports: [
    AppComponent,
    OverviewComponent,
    DetailviewComponent,
    ThermostatTileComponent
  ],
  providers: [
    StoreService,
    ThermostatService,
    DataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
